import time

def pierwsza(a):
    for i in range(2, int(a**0.5) + 1):
        if (a % i == 0):
            return False
    return True

#------------------------------------

czas1 = time.time()

for i in range (1, 50001):
    if pierwsza(i):
        print(i)

czas2 = time.time()
print(czas2-czas1)

# Nieoptymalny: 3.602724075317383
# //2 + 1: 1.8584892749786377
# sqrt(): 0.050837039947509766