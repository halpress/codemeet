import time
def pierwsza(a):
	for x in range(2,   int(a**0.5)+1   ):
		if a%x==0:
			return False
	return True

def polpierwsza(a):
	dzielniki=[]
	for d in range(2,(a//2)+1):
		if a%d==0:
			if pierwsza(d):
				dzielniki.append(d)
	for x in dzielniki:
		for y in dzielniki:
			if x*y==a:
				return True
	return False
#---------------------------------------

czas1=time.time()
for z in range(2,500):
	if polpierwsza(z):
		print(z)
czas2=time.time()
print(czas2-czas1)
