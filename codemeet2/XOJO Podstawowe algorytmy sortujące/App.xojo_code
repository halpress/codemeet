#tag Class
Protected Class App
Inherits ConsoleApplication
	#tag Event
		Function Run(args() as String) As Integer
		  Var tab() As Integer = stworzTablice(20, 20)
		  Var tab_bubble() As Integer = kopiujTablice(tab)
		  Var czas_poczatek As Integer
		  Var czas_koniec As Integer
		  Var czas As Integer
		  Var a As String
		  
		  wyswietlTablice(tab_bubble)
		  
		  czas_poczatek = System.Microseconds
		  tab_bubble = sortowanieBabelkowe(tab_bubble)
		  czas_koniec = System.Microseconds
		  
		  czas=czas_koniec - czas_poczatek
		  
		  wyswietlTablice(tab_bubble)
		  Print(czas.ToString)
		  
		  a = Input
		End Function
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Function kopiujTablice(staraTablica() as Integer) As Integer()
		  Var nowaTablica() As Integer
		  
		  For Each x As Integer In staraTablica
		    nowaTablica.Append(x)
		  Next
		  
		  Return nowaTablica
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function sortowanieBabelkowe(tablica() as Integer) As Integer()
		  For i As Integer = 0 To tablica.LastIndex
		    For i_lewy As Integer = 0 To tablica.LastIndex - i -1
		      Var i_prawy As Integer = i_lewy + 1
		      
		      If tablica(i_lewy) > tablica(i_prawy) Then
		        //zamieniamy
		        zamien(tablica, i_lewy, i_prawy)
		      End If
		      
		    Next
		  Next
		  
		  Return tablica
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function stworzTablice(rozmiar as Integer, zakres as Integer) As Integer()
		  Var tab() As Integer
		  
		  For i As Integer = 0 To rozmiar
		    tab.Add(System.Random.InRange(0, zakres))
		  Next
		  
		  Return tab
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub wyswietlTablice(tab() as Integer)
		  Var wiersz As String
		  
		  For Each liczba As Integer In tab
		    wiersz=wiersz + liczba.ToString + ", "
		  Next
		  
		  wiersz=wiersz+EndOfLine
		  Print(wiersz)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub zamien(tablica() as Integer, i_lewy as Integer, i_prawy as Integer)
		  Var temp As Integer
		  
		  If i_lewy <> i_prawy Then
		    temp = tablica(i_lewy)
		    tablica(i_lewy) = tablica(i_prawy)
		    tablica(i_prawy) = temp
		  End If
		End Sub
	#tag EndMethod


	#tag ViewBehavior
	#tag EndViewBehavior
End Class
#tag EndClass
