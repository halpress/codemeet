#tag Class
Protected Class App
Inherits WebApplication
	#tag Event
		Function HandleURL(Request As WebRequest, Response As WebResponse) As Boolean
		  Const domyslnaOdpowiedz As String = "To jest prosta aplikacja obsługująca format JSON."
		  Var sciezka As String = Request.Path
		  
		  // Sprawdzamy po ścieżce requesta jakie polecenie wykonać
		  
		  If sciezka = "dodajOsobe" Then
		    dodajOsobe(Request.Body, Response)
		  ElseIf sciezka = "pobierzOsoby" Then
		    pobierzOsoby(Response)
		  ElseIf sciezka = "wyczyscOsoby" Then
		    wyczyscOsoby(Response)
		  Else
		    Response.Status = 202
		    Response.Write(domyslnaOdpowiedz)
		  End If
		  
		  Return True
		End Function
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub dodajOsobe(formularz as String, odpowiedzFront as WebResponse)
		  Const bladFormularz As String = "Nie można odczytać formularza"
		  Const bladJSON As String = "Błędny format JSON'a"
		  
		  Var formularzJSON As JSONItem
		  Var nowaOsoba As Osoba
		  
		  // Próbujemy zamienić zawartość body z requesta na JSON'a
		  Try
		    formularzJSON = New JSONItem(formularz)
		  Catch
		    odpowiedzFront.Status = 400
		    odpowiedzFront.Write(bladFormularz)
		    Return 
		  End Try
		  
		  // Tworzymy osobę na podstawie JSON'a
		  nowaOsoba = Osoba.zczytajJSON(formularzJSON)
		  
		  // Jeżeli osoba jest pusta (Nil), to nie udało się utworzyć osoby, czyli jest błąd
		  If nowaOsoba = Nil Then
		    odpowiedzFront.Status = 400
		    odpowiedzFront.Write(bladJSON)
		    Return 
		  End If
		  
		  // Jeżeli jest wszystko okej, to dodajemy nową osobę
		  tabOsoby.Add(nowaOsoba)
		  odpowiedzFront.Status = 200
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub pobierzOsoby(odpowiedzFront as WebResponse)
		  Const brakOsob As String = "Brak osób"
		  Var odpowiedzJSON As New JSONItem
		  Var osobyJSON As New JSONItem
		  
		  // Jeżeli nie ma osób na backendzie, to zwracamy błąd
		  If tabOsoby.Count = 0 Then
		    odpowiedzFront.Write(brakOsob)
		    odpowiedzFront.Status = 404
		    Return
		  End If
		  
		  // Pakujemy każdą osobę do JSON'a
		  For Each os As Osoba In tabOsoby
		    osobyJSON.Add(os.utworzJSON)
		  Next
		  
		  // Zwracamy tablicę osób dla klucza 'osoby' 
		  odpowiedzJSON.Value("osoby") = osobyJSON
		  odpowiedzFront.Write(odpowiedzJSON.ToString)
		  odpowiedzFront.Status = 200
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub wyczyscOsoby(response as WebResponse)
		  Const usunietoOdpowiedz As String = "Usunięto"
		  
		  tabOsoby.RemoveAll
		  Response.Status = 200
		  Response.Write(usunietoOdpowiedz)
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		tabOsoby() As Osoba
	#tag EndProperty


	#tag ViewBehavior
	#tag EndViewBehavior
End Class
#tag EndClass
