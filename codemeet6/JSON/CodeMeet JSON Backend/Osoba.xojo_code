#tag Class
Protected Class Osoba
	#tag Method, Flags = &h0
		Sub Constructor(imie as String, nazwisko as String, dataUr as DateTime)
		  Me.imie = imie
		  Me.nazwisko = nazwisko
		  me.dataUr = dataUr
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(imie as String, nazwisko as String, dataUr as DateTime, wzrost as Integer)
		  Me.Constructor(imie, nazwisko, dataUr)
		  me.wzrost = wzrost
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function dataUr() As DateTime
		  return mDataUr
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub dataUr(assigns value as DateTime)
		  mDataUr = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function imie() As String
		  return mImie
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub imie(assigns value as String)
		  mImie = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function nazwisko() As String
		  return mNazwisko
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub nazwisko(assigns value as String)
		  mNazwisko = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function utworzJSON() As JSONItem
		  Var plikJSON As New JSONItem
		  
		  plikJSON.Value("imie") = imie
		  plikJSON.Value("nazwisko") = nazwisko
		  plikJSON.Value("dataUrodzenia") = dataUr.SQLDate
		  If wzrost > 0 Then
		    plikJSON.Value("wzrost") = wzrost
		  End If
		  
		  Return plikJSON
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function wzrost() As Integer
		  return mWzrost
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub wzrost(assigns value as Integer)
		  mWzrost = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function zczytajJSON(osobaSlownik as JSONItem) As Osoba
		  Var os As Osoba
		  
		  // Sprawdzamy czy dany słownik ma takie klucze jak imię, nazwisko, data urodzenia (obowiązkowe)
		  If osobaSlownik.HasKey("imie") And osobaSlownik.HasKey("nazwisko") And osobaSlownik.HasKey("dataUrodzenia") Then
		    
		    os = New Osoba( _
		    osobaSlownik.Value("imie").StringValue.DefineEncoding(Encodings.UTF8), _
		    osobaSlownik.Value("nazwisko").StringValue.DefineEncoding(Encodings.UTF8), _
		    osobaSlownik.value("dataUrodzenia").DateValue)
		    
		    // Sprawdzamy czy dany słownik ma klucz wzrost (nieobowiązkowy)
		    If osobaSlownik.HasKey("wzrost")  Then
		      os.wzrost = osobaSlownik.Value("wzrost").IntegerValue
		    End If
		    
		  End If
		  
		  Return os
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private mDataUr As DateTime
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mImie As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mNazwisko As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mWzrost As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
