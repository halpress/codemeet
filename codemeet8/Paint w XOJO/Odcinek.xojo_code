#tag Class
Protected Class Odcinek
	#tag Method, Flags = &h0
		Sub dodajPunkt(p as Point)
		  tab.Add(p)
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		gruboscOdcinka As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		kolorOdcinka As Color
	#tag EndProperty

	#tag Property, Flags = &h0
		tab() As Point
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
