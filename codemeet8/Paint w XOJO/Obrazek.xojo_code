#tag Class
Protected Class Obrazek
	#tag Method, Flags = &h0
		Sub Constructor()
		  czyRysujemy=False
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub dodajNowyOdcinek(kolor as Color, grubosc as double)
		  Var nowyOdcinek As New Odcinek
		  nowyOdcinek.kolorOdcinka=kolor
		  nowyOdcinek.gruboscOdcinka=grubosc
		  odcinki.add(nowyOdcinek)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub dodajPunkt(p as Point)
		  odcinki(odcinki.LastIndex).dodajPunkt(p)
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		czyRysujemy As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		odcinki() As Odcinek
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
